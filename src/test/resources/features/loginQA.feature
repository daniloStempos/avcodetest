Feature: Login QA

  @LoginNeeded
  Scenario: User logs in successfully
    Given I am in login page
    When I fill in the login fields correctly
      | email                    | password |
      | danilotempos@hotmail.com | 12345678 |
    Then I should see Home Page

