package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/main/resources/features"},
        glue = {"StepDefinition"},
        monochrome = true,
        snippets = SnippetType.CAMELCASE,
        strict = true
)

//public class RunCukesTest {}

public class run {

}
