import data.User;
import org.junit.Before;

public class Funcionalidades {

    public static void preCondicao() throws Exception {
        PageObject.preCondicao();
    }


    public static void signInQATest(User user){
        PageObject.clickSignInQATest();
        PageObject.loginQAtest(user);

    }

    public static void createNewTask(User user){
        PageObject.clickMyTasks();
        PageObject.inputNewTask(user);
        PageObject.clickButtonAddTask();
    }

    public static void createSubtask(User user){
        PageObject.clickButtonManageSubtasks();
        PageObject.inputNewSubtask(user);
        PageObject.inputDueDate();
        PageObject.clickButtonAddSubTask();
        PageObject.clickButtonClosePopupSubtask();

    }

    public static void messageLoginSuccesfully(){
        PageObject.validateLoginSuccessfully();
    }





}
