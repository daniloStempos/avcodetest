import data.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PageObject {

    public static WebDriver driver;

    public static void preCondicao() throws Exception {

        // Abrir o Google Chrome
        System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");
        driver = new ChromeDriver();

        // Maximizar o Browser
        driver.manage().window().maximize();

        // Acessar a URL de QA da Avenue Code
        driver.get("https://qa-test.avenuecode.com/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    /**
     * Click button "Sign in" at Home Page
     */
    public static void clickSignInQATest(){
        driver.findElement(By.xpath("//*[@class = 'navbar-collapse collapse']/ul[2]/li[1]/a")).click();

    }

    public static void loginQAtest(User user){
        driver.findElement(By.id("user_email")).clear();
        driver.findElement(By.id("user_email")).sendKeys(user.email);
        driver.findElement(By.id("user_password")).sendKeys(user.password);
        driver.findElement(By.xpath("//*[@value = 'Sign in']")).click();

    }

    public static void validateLoginSuccessfully() {
        String txtSuccessLogin = driver.findElement(By.xpath("//*[contains(div, 'successfully')]")).getText();

        try {
            if (txtSuccessLogin.contains("successfully")) {
                System.out.println(txtSuccessLogin);

            } else {

            }

        } catch (Exception errorLogin) {
            System.out.println("Login unsuccessfully");
        }

    }

    /**
     * Click on button "My tasks"
     */
    public static void clickMyTasks(){
        driver.findElement(By.xpath("//*[@class = 'navbar-collapse collapse']/ul[1]/li[2]/a")).click();

    }

    public static void inputNewTask(User user){
        driver.findElement(By.id("new_task")).clear();
        driver.findElement(By.id("new_task")).sendKeys(user.task);

    }

    public static void clickButtonAddTask(){
        driver.findElement(By.xpath("//*[@class = 'input-group-addon glyphicon glyphicon-plus']")).click();
    }

    public static void clickButtonManageSubtasks(){
        driver.findElement(By.xpath("//*[@class = 'table']/tbody/tr[1]/td[4]/button")).click();

    }

    public static void inputNewSubtask(User user){
        driver.findElement(By.id("new_sub_task")).clear();
        driver.findElement(By.id("new_sub_task")).sendKeys(user.subtask);
    }


    public static void inputDueDate(){
        Date dateHourNow = new Date();
        String dateNow = new SimpleDateFormat("MM/dd/yyyy").format(dateHourNow);

        driver.findElement(By.id("dueDate")).clear();
        driver.findElement(By.id("dueDate")).sendKeys(dateNow);

    }

    public static void clickButtonAddSubTask(){
        driver.findElement(By.id("add-subtask")).click();
    }

    public static void clickButtonClosePopupSubtask(){
        driver.findElement(By.xpath("//*[@class = 'modal-footer ng-scope']/button")).click();
    }

    public static void clickCheckboxSubtaskDone(){
        driver.findElement(By.xpath("//*[@ng-show = 'task.sub_tasks.length']/table/tbody/tr[1]/td[1]/input")).click();
    }



}
