import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.User;

public class StepDefinition {


    @Given("^I am in login page$")
    public void iAmInTheLoginPage() throws Exception {
        Funcionalidades.preCondicao();

    }

    @When("^I fill in the login fields correctly$")
    public void iFillInDetails(User user) {
        Funcionalidades.signInQATest(user);

    }

    @Then("^I should see Home Page$")
    public void iShouldSeeHomePage(){
        Funcionalidades.messageLoginSuccesfully();

    }


}
